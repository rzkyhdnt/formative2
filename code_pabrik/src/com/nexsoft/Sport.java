package com.nexsoft;

import java.util.Scanner;

public class Sport extends Motorcycle{

    @Override
    void getTransmisi() {
        System.out.println("Motor sport menggunakan transmisi manual yang menggunakan gear atau mengoper 'gigi' dan menggunakan kopling.");
    }

    @Override
    void getSpeed() {
        kapasitasMesin = 1650;
        System.out.println("Kapasitas mesin berkisar > " + kapasitasMesin + " cc");
        System.out.println("Kecepatan maksimal berkisar > 300 km/h");
    }

    @Override
    void getDimensi() {
        if(merek == "Honda"){
            System.out.println("Dimensi " + merek + "       : 1.983 x 694 x 1.038 mm");
        } else if(merek == "Suzuki"){
            System.out.println("Dimensi " + merek + "       : 2.020 x 700 x 1.075 mm");
        } else if(merek == "Tesla"){
            System.out.println("Dimensi " + merek + "       : 1.990 x 725 x 1.135 mm");
        } else {
            System.out.println("Motor Sport tidak terdaftar!");
        }
    }

    @Override
    void getBahanBakar() {
        if(merek == "Honda" || merek=="Suzuki"){
            System.out.println("Bahan Bakar         : Bensin");
        } else if(merek == "Tesla"){
            System.out.println("Bahan Bakar         : Listrik");
        } else {
            System.out.println("Bahan bakar tidak ditemukan!");
        }
    }

    @Override
    void getHarga() {
        Scanner valuta = new Scanner(System.in);
        System.out.println("\nHarga berdasarkan valuta");
        System.out.println("1. USD");
        System.out.println("2. IDR");
        System.out.println("Masukkan pilihan : ");
        int currency =  valuta.nextInt();

        if(merek == "Honda"){
            price = 45000000;
        } else if(merek == "Suzuki"){
            price = 40000000;
        } else if(merek == "Tesla"){
            price = 200000000;
        }

        switch (currency){
            case 1 : convertIdrToUsd(); break;
            case 2 : System.out.println("Harga Jual        : Rp. " + price); break;
            default: System.out.println("Harga tidak terdaftar");
        }
    }

    void printResult(String merek){
        this.merek = merek;
        System.out.println("\nJenis kendaraan     : Motor Sport Merk " + merek);
        getDimensi();
        getBahanBakar();
        getTransmisi();
        getSpeed();
        getHarga();
    }
}
