package com.nexsoft;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Factory factory = new Factory();
        factory.setName("Astra");

        System.out.println("\n***********************Motor***********************");
        Matic beat = new Matic();
        beat.printResult("Honda");

        System.out.println("\n\n***********************Mobil***********************");
        Sedan camry = new Sedan();
        camry.printResult("Honda");
    }
}
