package com.nexsoft;

abstract class Motorcycle extends Factory {
    protected String name, merek;
    protected int price, kapasitasMesin;
    final int kurs = 14500;

    abstract void getTransmisi();
    abstract void getSpeed();
    abstract void getDimensi();
    abstract void getBahanBakar();
    abstract void getHarga();

    protected void convertIdrToUsd(){
        double  totalPrice;
        totalPrice = price / kurs;
        System.out.print("Harga Jual        : $" + totalPrice);
    }
}
