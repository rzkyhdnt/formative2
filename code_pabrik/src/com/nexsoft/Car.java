package com.nexsoft;

abstract class Car extends Factory {
    protected String name, valuta, merek;
    protected int price, kapasitasMesin, tenaga, torsi, jok;
    final int kurs = 14500;

    abstract void getPerforma();
    abstract void getKapasitas();
    abstract void getTransmisi();
    abstract void getMesin();
    abstract void getHarga();

    protected void convertIdrToUsd(){
        double  totalPrice;
        totalPrice = price / kurs;
        System.out.print("Harga Jual        : $" + totalPrice);
    }

}
