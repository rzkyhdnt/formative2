package com.nexsoft;

public class Factory {
    protected String name, merk;

    public void setName(String name){
        this.name = name;
        getProfile();
    }

    public void getProfile(){
        if(name == "Astra"){
            merk = "Honda";
            System.out.println("---------------PT ASTRA INTERNATIONAL---------------");
            System.out.println("PT Astra International Tbk didirikan di Jakarta pada tahun 1957 sebagai sebuah\nperusahaan perdagangan umum dengan nama Astra International Inc");
            System.out.println("Merk dagang dari Astra adalah " + merk);
        } else if(name == "Suzuki"){
            merk = "Suzuki";
            System.out.println("---------------PT SUZUKI INDONESIA---------------");
            System.out.println("Suzuki Indonesia merupakan kelompok usaha yang bergerak dibidang industri otomotif\nyang memproduksi, memasarkan, memperniagakan motor, mobil dan motor tempel (outboard- motor).");
            System.out.println("Merk dagang dari Suzuki adalah " + merk);
        } else if(name == "Tesla"){
            merk = "Tesla";
            System.out.println("---------------TESLA,Inc---------------");
            System.out.println("Tesla Motors adalah sebuah perusahaan otomotif dan penyimpanan energi asal Amerika\nSerikat yang didirikan oleh Elon Musk, Martin Eberhard, Marc Tarpenning, JB Straubel dan Ian Wright,\nserta berbasis di Palo Alto, California.");
            System.out.println("Merk dagang dari Tesla adalah " + merk);
        } else {
            System.out.println("Nama factory tidak terdaftar!");
        }
    }
}
