package com.nexsoft;

import java.util.Scanner;

public class Matic extends Motorcycle {
    @Override
    void getTransmisi() {
        System.out.println("Motor matic menggunakan transmisi otomatis.");
    }

    @Override
    void getSpeed() {
        kapasitasMesin = 400;
        System.out.println("Kapasitas mesin berkisar max " + kapasitasMesin + " cc");
        System.out.println("Kecepatan maksimal berkisar > 150 km/h");
    }

    @Override
    void getDimensi() {
        if(merek == "Honda"){
            System.out.println("Dimensi " + merek + "       : 50 x 55,1 mm");
        } else if(merek == "Suzuki"){
            System.out.println("Dimensi " + merek + "       : 60 x 53 mm");
        } else if(merek == "Tesla"){
            System.out.println("Dimensi " + merek + "       : 48 x 54 mm");
        } else {
            System.out.println("Motor Sport tidak terdaftar!");
        }
    }

    @Override
    void getBahanBakar() {
        if(merek == "Honda" || merek=="Suzuki"){
            System.out.println("Bahan Bakar         : Bensin");
        } else if(merek == "Tesla"){
            System.out.println("Bahan Bakar         : Listrik");
        } else {
            System.out.println("Bahan bakar tidak ditemukan!");
        }
    }

    @Override
    void getHarga() {
        Scanner valuta = new Scanner(System.in);
        System.out.println("\nHarga berdasarkan valuta");
        System.out.println("1. USD");
        System.out.println("2. IDR");
        System.out.println("Masukkan pilihan : ");
        int currency =  valuta.nextInt();

        if(merek == "Honda"){
            price = 16000000;
        } else if(merek == "Suzuki"){
            price = 14000000;
        } else if(merek == "Tesla"){
            price = 98000000;
        }

        switch (currency){
            case 1 : convertIdrToUsd(); break;
            case 2 : System.out.println("Harga Jual        : Rp. " + price); break;
            default: System.out.println("Harga tidak terdaftar");
        }
    }

    void printResult(String merek){
        this.merek = merek;
        System.out.println("\nJenis kendaraan     : Motor Matic Merk " + merek);
        getDimensi();
        getBahanBakar();
        getTransmisi();
        getSpeed();
        getHarga();
    }
}
