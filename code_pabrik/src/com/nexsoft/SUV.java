package com.nexsoft;

import java.util.Scanner;

public class SUV extends Car{

    @Override
    void getPerforma() {
        System.out.println("Performa");
        if(merek == "Honda"){
            kapasitasMesin = 1500;
            tenaga = 130;
            torsi = 150;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else if(merek == "Suzuki"){
            kapasitasMesin = 1550;
            tenaga = 140;
            torsi = 145;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else if(merek == "Tesla"){
            kapasitasMesin = 1400;
            tenaga = 148;
            torsi = 150;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getKapasitas() {
        System.out.println("\nKapasitas");
        if(merek == "Honda"){
            jok = 8;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else if(merek == "Suzuki"){
            jok = 6;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else if(merek == "Tesla"){
            jok = 4;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getTransmisi() {
        System.out.println("Gearbox         : 4-Speed");
    }

    @Override
    void getMesin() {
        int silinder, katup;
        System.out.println("\nDetil Mesin");
        if(merek == "Honda"){
            silinder = 4;
            katup = 2;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else if(merek == "Suzuki"){
            silinder = 4;
            katup = 2;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else if(merek == "Tesla"){
            silinder = 4;
            katup = 4;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getHarga() {
        Scanner valuta = new Scanner(System.in);
        System.out.println("\nHarga berdasarkan valuta");
        System.out.println("1. USD");
        System.out.println("2. IDR");
        System.out.println("Masukkan pilihan : ");
        int currency =  valuta.nextInt();

        if(merek == "Honda"){
            price = 450000000;
        } else if(merek == "Suzuki"){
            price = 400000000;
        } else if(merek == "Tesla"){
            price = 800000000;
        }

        switch (currency){
            case 1 : convertIdrToUsd(); break;
            case 2 : System.out.println("Harga Jual        : Rp. " + price); break;
            default: System.out.println("Harga tidak terdaftar");
        }
    }

    void printResult(String merek){
        this.merek = merek;
        System.out.println("\nJenis kendaraan         : Mobil SUV Merk " + merek);
        getPerforma();
        getKapasitas();
        getMesin();
        getHarga();
    }
}