package com.nexsoft;

import java.util.Scanner;

public class Sedan extends Car{
    @Override
    void getPerforma() {
        System.out.println("Performa");
        if(merek == "Honda"){
            kapasitasMesin = 1497;
            tenaga = 118;
            torsi = 145;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else if(merek == "Suzuki"){
            kapasitasMesin = 1400;
            tenaga = 113;
            torsi = 132;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else if(merek == "Tesla"){
            kapasitasMesin = 1200;
            tenaga = 110;
            torsi = 140;
            System.out.println("Kapasitas Mesin         : " + kapasitasMesin + " cc");
            System.out.println("Tenaga                  : " + tenaga + " hp");
            System.out.println("Torsi                   : " + torsi + " hp");
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getKapasitas() {
        System.out.println("\nKapasitas");
        if(merek == "Honda"){
            jok = 4;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else if(merek == "Suzuki"){
            jok = 4;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else if(merek == "Tesla"){
            jok = 2;
            System.out.println("Jumlah Tempat Duduk     : " + jok + " buah");
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getTransmisi() {
        System.out.println("Gearbox         : 5-Speed");
    }

    @Override
    void getMesin() {
        int silinder, katup;
        System.out.println("\nDetil Mesin");
        if(merek == "Honda"){
            silinder = 4;
            katup = 4;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else if(merek == "Suzuki"){
            silinder = 4;
            katup = 4;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else if(merek == "Tesla"){
            silinder = 2;
            katup = 2;
            System.out.println("Jumlah Silinder         : " + silinder);
            System.out.println("Jumlah Katup            : " + silinder);
        } else {
            System.out.println("Mobil sedan tidak terdaftar!");
        }
    }

    @Override
    void getHarga() {
        Scanner valuta = new Scanner(System.in);
        System.out.println("\nHarga berdasarkan valuta");
        System.out.println("1. USD");
        System.out.println("2. IDR");
        System.out.println("Masukkan pilihan : ");
        int currency =  valuta.nextInt();

        if(merek == "Honda"){
            price = 600000000;
        } else if(merek == "Suzuki"){
            price = 500000000;
        } else if(merek == "Tesla"){
            price = 1100000000;
        }

        switch (currency){
            case 1 : convertIdrToUsd(); break;
            case 2 : System.out.println("Harga Jual        : Rp. " + price); break;
            default: System.out.println("Harga tidak terdaftar");
        }
    }

    void printResult(String merek){
        this.merek = merek;
        System.out.println("\nJenis kendaraan         : Mobil Sedan Merk " + merek);
        getPerforma();
        getKapasitas();
        getMesin();
        getHarga();
    }

}
