package com.nexsoft;

import java.util.Scanner;

public class Bebek extends Motorcycle{
    @Override
    void getTransmisi() {
        System.out.println("Motor bebek menggunakan transmisi manual yang menggunakan gear atau mengoper 'gigi'");
    }

    @Override
    void getSpeed() {
        kapasitasMesin = 600;
        System.out.println("Kapasitas mesin berkisar max " + kapasitasMesin + " cc");
        System.out.println("Kecepatan maksimal berkisar > 220 km/h");
    }

    @Override
    void getDimensi() {
        if(merek == "Honda"){
            System.out.println("Dimensi " + merek + "       : 52,4 x 57,9 mm");
        } else if(merek == "Suzuki"){
            System.out.println("Dimensi " + merek + "       : 54,2 x 56,7 mm");
        } else if(merek == "Tesla"){
            System.out.println("Dimensi " + merek + "       : 51,6 x 57,7 mm");
        } else {
            System.out.println("Motor Sport tidak terdaftar!");
        }
    }

    @Override
    void getBahanBakar() {
        if(merek == "Honda" || merek=="Suzuki"){
            System.out.println("Bahan Bakar         : Bensin");
        } else if(merek == "Tesla"){
            System.out.println("Bahan Bakar         : Listrik");
        } else {
            System.out.println("Bahan bakar tidak ditemukan!");
        }
    }

    @Override
    void getHarga() {
        Scanner valuta = new Scanner(System.in);
        System.out.println("\nHarga berdasarkan valuta");
        System.out.println("1. USD");
        System.out.println("2. IDR");
        System.out.println("Masukkan pilihan : ");
        int currency =  valuta.nextInt();

        if(merek == "Honda"){
            price = 20000000;
        } else if(merek == "Suzuki"){
            price = 19000000;
        } else if(merek == "Tesla"){
            price = 112000000;
        }

        switch (currency){
            case 1 : convertIdrToUsd(); break;
            case 2 : System.out.println("Harga Jual        : Rp. " + price); break;
            default: System.out.println("Harga tidak terdaftar");
        }
    }

    void printResult(String merek){
        this.merek = merek;
        System.out.println("\nJenis kendaraan     : Motor Bebek Merk " + merek);
        getDimensi();
        getBahanBakar();
        getTransmisi();
        getSpeed();
        getHarga();
    }
}
